""" Semi-automatic tool to add Other Database URL Advanced Relationships to albums listed on TheSession.org to MusicBrainz.

By Jesse Weinstein <jesse@wefu.org>
This version is released under the GPL version 2 or later. 
Later versions may switch to another (GPL-compatible) license.

It requires *both* python-musicbrainzngs (the webservice library) and musicbrainz-bot (the scraping bot library).

You will probably need to change the paths appended to sys.path below.

The code expects an object, named cfg, containing three attributes (username, password & email) to exist in a separate module called private_config. You will need to create that module.
"""

import functools

def memoize(obj):
    """ Modified from https://wiki.python.org/moin/PythonDecoratorLibrary#Memoize
        as of Sat 29 Mar 2014 04:31:29 PM PDT.

        Added force=True feature to force updating the cache."""
    cache = obj.cache = {}

    @functools.wraps(obj)
    def memoizer(*args, **kwargs):
        force = False
        if kwargs.has_key('force'):
            force = kwargs['force']
            del kwargs['force']
        key = str(args) + str(kwargs)
        if force or key not in cache:
            cache[key] = obj(*args, **kwargs)
        return cache[key]
    return memoizer

import time
from functools import wraps

def retry(ExceptionToCheck, tries=4, delay=3, backoff=2, logger=None):
    """Retry calling the decorated function using an exponential backoff.

    http://www.saltycrane.com/blog/2009/11/trying-out-retry-decorator-python/
    original from: http://wiki.python.org/moin/PythonDecoratorLibrary#Retry

    :param ExceptionToCheck: the exception to check. may be a tuple of
        exceptions to check
    :type ExceptionToCheck: Exception or tuple
    :param tries: number of times to try (not retry) before giving up
    :type tries: int
    :param delay: initial delay between retries in seconds
    :type delay: int
    :param backoff: backoff multiplier e.g. value of 2 will double the delay
        each retry
    :type backoff: int
    :param logger: logger to use. If None, print
    :type logger: logging.Logger instance
    """
    def deco_retry(f):

        @wraps(f)
        def f_retry(*args, **kwargs):
            mtries, mdelay = tries, delay
            while mtries > 1:
                try:
                    return f(*args, **kwargs)
                except ExceptionToCheck, e:
                    msg = "%s, Retrying in %d seconds..." % (str(e), mdelay)
                    if logger:
                        logger.warning(msg)
                    else:
                        print msg
                    time.sleep(mdelay)
                    mtries -= 1
                    mdelay *= backoff
            return f(*args, **kwargs)

        return f_retry  # true decorator

    return deco_retry

import re, urllib2, random, sys, logging

sys.path.append('/home/jesse/FreeProjects/musicbrainz-bot')
import editing
sys.path.append('/home/jesse/FreeProjects/python-musicbrainzngs')
import musicbrainzngs

from private_config import cfg

class TheSession:
    title_pat=re.compile('<h1>(.*)</h1>');
    artist_pat=re.compile('<h2>By <a href="/recordings/artists/[0-9]+">(.*)</a></h2>');
    def __init__(self):
        musicbrainzngs.auth(cfg.username, cfg.password)
        musicbrainzngs.set_useragent("Jesse_semi_automatic_linking_app", "0.1",cfg.email)
        self.mb = editing.MusicBrainzClient(cfg.username, cfg.password, 'https://musicbrainz.org')
    
    def make_url(self,n):
        return 'http://thesession.org/recordings/'+`n`

    @memoize
    @retry(urllib2.HTTPError, tries=2, delay=10, backoff=2, logger=logging.getLogger())
    def lookup_on_thesession(self, n):
        logging.debug(n)
        txt=urllib2.urlopen(self.make_url(n)).read()
        
        r=self.title_pat.search(txt)
        a=self.artist_pat.search(txt)
        if (r and a):
            return (r.group(1), a.group(1))
        logging.warning(`n`+' failed!')
        logging.debug(txt.splitlines()[:3])
        return (None, None)
    
    
    @memoize
    def lookup_by_name(self, n):
        r,a = self.lookup_on_thesession(n)
        if (r and a):
            return musicbrainzngs.search_release_groups(release=r, artist=a, strict=True)['release-group-list']
        return []
    
    @memoize
    def lookup_by_url(self, n):
        try:
            return musicbrainzngs.browse_urls(self.make_url(n), includes=['release-group-rels', 'release-rels'])['url']
        except musicbrainzngs.musicbrainz.ResponseError, e:
            if (e.cause.code==404):
                return {'release_group-relation-list':[], 'release-relation-list':[]}
            raise e

    def has_url_relations(self, n):
        return self.lookup_by_url(n)['release_group-relation-list'] or self.lookup_by_url(n)['release-relation-list']

    def already_entered(self, n):
        bn=[y['id'] for y in self.lookup_by_name(n)]; bn.sort()
        bu=[y['target'] for y in self.lookup_by_url(n)['release_group-relation-list']]; bu.sort()
        return bn and bn==bu

    def dump(self, min_n, max_n):
        """ Just for debugging. Currently unused."""
        print '\n'.join([(
            `self.lookup_on_thesession(n)`+'::'+
            '\n\t'.join([`(y['title'], [z['artist']['name'] for z in y['artist-credit']])`
                         for y in self.lookup_by_name(n)]))
                         for n in range(min_n, max_n)])

    def display_from_thesession(self,n):
        r,a = self.lookup_on_thesession(n)
        print 'For #'+`n`+', on TheSession, "'+r+'" by "'+a+'"'
    def display_from_mb(self,n):
        s = self.lookup_by_name(n)
        def artistDisp(aq):
            if type(aq)!=dict:
                return aq
            aa=aq['artist']
            return '"'+aa['name']+'"'+(aa.has_key('disambiguation') and ' ('+aa['disambiguation']+')' or '')

        print 'For #'+`n`+', on MB,',
        if len(s)==0:
            print ' None found.'
        elif len(s)>1:
            print ' Multiple possible releases found.'
        else:
            print '"'+s[0]['title']+'" by',
            print ''.join([artistDisp(a) for a in s[0]['artist-credit']])

    def maybe_add_ar(self, n, just_check=False):
        print '#'+`n`,
        if self.already_entered(n):
            print 'has already been entered.'
        elif self.has_url_relations(n):
            print 'is already used elsewhere.'
        elif len(self.lookup_by_name(n))==0:
            print 'is not found in MB.'
        elif len(self.lookup_by_name(n))>1:
            print 'does not match to exactly one entry in MB.'
        else:
            print 'might be a valid entry.\n'
            self.display_from_thesession(n)
            self.display_from_mb(n)
            if not just_check and raw_input('Add URL relationship? ').lower()=='y':
                self.mb.add_url('release_group', self.lookup_by_name(n)[0]['id'], 96, self.make_url(n), 'Semi-automatically added based on matching release title and artist.')
                self.lookup_by_url(n, force=True) #Update cache
                print 'URL relationship added.'

    def maybe_add_ars(self, min_n, max_n, shuffle=True, just_check=False):
        """ Main function. """
        rng=range(min_n, max_n)
        if shuffle:
            random.shuffle(rng)
        i=0
        for n in rng:
            try:
                if shuffle:
                    print i,'::',
                i += 1
                self.maybe_add_ar(n, just_check)
            except urllib2.HTTPError:
                print 'HTTP Error ...'
